import React, { useState, useEffect, useRef } from "react";
import ReactDOM from "react-dom";

import "./styles.css";

// Simulated autocomplete data (replace with your API call)
const mockAutocompleteData = [
  "Apple",
  "Google",
  "Facebook",
  "Instagram",
  "TikTok",
  "Twitter",
  "Microsoft",
  "Amazon",
  "IBM",
  "Wallmart",
  "Tesla",
  "Coca Cola"
];

const SearchEngine = () => {
  const [cursor, setCursor] = useState(-1);
  const [searchTerm, setSearchTerm] = useState("");
  const [autocompleteItems, setAutocompleteItems] = useState([]);
  const [searchResults, setSearchResults] = useState([]);
  // For a real SearchEngine we should use localStorage but for demonstration purposes this will do
  const [accessedItems, setAccessedItems] = useState([]);
  const autocompleteRef = useRef(null);
  const inputRef = useRef(null);

  const inputFocus = document.activeElement === inputRef.current;

  const onSearchTermChange = (term) => {
    const filteredItems = mockAutocompleteData.filter((item) =>
      item.toLowerCase().includes(term.toLowerCase())
    );

    setAutocompleteItems(filteredItems.slice(0, 10));
  };

  useEffect(() => {
    if (inputFocus) {
      onSearchTermChange(searchTerm);
    }
  }, [searchTerm]);

  const handleSearch = (selectedItem) => {
    setSearchTerm(selectedItem);
    // Simulate search results
    setSearchResults(
      [...Array(Math.floor(Math.random() * 15 + 1)).keys()].map((nr) => ({
        id: nr,
        link: `https://picsum.photos/200/300?random=1`,
        title: `Search Result ${nr + 1}`,
        description: `Description for result related to ${selectedItem} ${
          nr + 1
        }`
      }))
    );
    setAccessedItems((prevItems) => [...new Set([...prevItems, selectedItem])]);
    setAutocompleteItems([]); // Hide the autocomplete
  };

  const handleClearHistory = (e) => {
    e.stopPropagation();
    setAccessedItems([]);
    inputRef.current.focus();
  };

  const handleKeyDown = (e) => {
    if (e.keyCode === 38 && cursor > 0) {
      setCursor(cursor - 1);
    } else if (e.keyCode === 40 && cursor < autocompleteItems.length - 1) {
      setCursor(cursor + 1);
    } else if (inputFocus && e.keyCode === 13) {
      handleSearch(autocompleteItems[cursor]);
      inputRef.current.blur();
    }
  };

  return (
    <>
      <div ref={autocompleteRef} className="search-engine">
        <input
          onKeyDown={handleKeyDown}
          ref={inputRef}
          type="text"
          placeholder="Search Google"
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
          onFocus={() => onSearchTermChange(searchTerm)}
          onBlur={(event) => {
            if (
              autocompleteRef.current &&
              !autocompleteRef.current.contains(event.relatedTarget)
            ) {
              setAutocompleteItems([]);
            }
          }}
          autoFocus
          className="search-input"
        />
        <ul className="autocomplete-list">
          {autocompleteItems.map((item, idx) => (
            <li
              tabIndex={idx}
              key={item}
              onMouseEnter={() => {
                setCursor(idx);
              }}
              onClick={() => handleSearch(item)}
              className={`autocomplete-item ${
                cursor === idx ? "autocomplete-item-selected" : ""
              }`}
            >
              {item}
              {accessedItems.includes(item) && (
                <button
                  onClick={handleClearHistory}
                  className="clear-history-button"
                >
                  x
                </button>
              )}
            </li>
          ))}
        </ul>
      </div>
      <div className="search-results">
        {!!searchResults.length && (
          <p>
            About {searchResults.length} results ({0.05 * searchResults.length}{" "}
            seconds)
          </p>
        )}
        {searchResults.map((result) => (
          <div key={result.id} className="search-result">
            <a href={result.link} className="search-result-title">
              {result.title}
            </a>
            <p className="search-result-description">{result.description}</p>
          </div>
        ))}
      </div>
    </>
  );
};

function App() {
  return (
    <div className="App">
      <SearchEngine />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
